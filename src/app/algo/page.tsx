'use client'

/* eslint-disable @typescript-eslint/no-loop-func */
/* eslint-disable prefer-const */
import React, { VFC } from 'react'

// const sortBubble = (array: number[]): number[] => {
//   let dummyArray = array

//   for (let i = 0; i < array.length; i++) {
//     // 後ろから１番目と２番目の数字を比較してソート
//     for (let index = dummyArray.length - 1; index >= i; index--) {
//       if (index - 1 < 0) {
//         break
//       }
//       const elementBack = dummyArray[index]
//       const elementFront = dummyArray[index - 1]
//       if (elementBack < elementFront) {
//         const tmp = dummyArray[index]
//         dummyArray[index] = dummyArray[index - 1]
//         dummyArray[index - 1] = tmp
//       }
//     }
//   }

//   return dummyArray
// }

// const choiceSort = (array: number[]): number[] => {
//   let dummyArray = array

//   // ソート済みの配列はそのまま
//   for (let i = 0; i < dummyArray.length; i++) {
//     let minNum: number | undefined = undefined
//     // 後ろから１番目と２番目の数字を比較してソート
//     for (let index = i; index < dummyArray.length; index++) {
//       if (minNum === undefined) {
//         minNum = dummyArray[index]
//       }
//       // minNumが大きければ、最小値を配列の一番最初に配置
//       if (minNum > dummyArray[index]) {
//         minNum = dummyArray[index]
//         dummyArray[index] = dummyArray[i]
//         dummyArray[i] = minNum
//       }
//     }
//   }

//   return dummyArray
// }

const Page: VFC = () => {
  // console.log(choiceSort([2, 1, 7, 4, 5]))
  return <>アルゴリズム練習用</>
}

export default Page
