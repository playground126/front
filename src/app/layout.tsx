'use client'

import React from 'react'
import { SamplePageSessionProvider } from '~/features/sample/shared/useSamplePage'

export default function RootLayout({
  // Layouts must accept a children prop.
  // This will be populated with nested layouts or pages
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="jp">
      <body>
        <SamplePageSessionProvider>{children}</SamplePageSessionProvider>
      </body>
    </html>
  )
}
