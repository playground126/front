'use client'

import React, { VFC } from 'react'
import { SampleAuthPage } from '~/features/sample/auth/SampleAuthPage'

const Page: VFC = () => <SampleAuthPage />

export default Page
