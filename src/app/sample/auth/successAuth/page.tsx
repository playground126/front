'use client'

import React, { VFC } from 'react'
import { useAuth } from '~/features/shared/auth/useAuth'
import { SuccessAuthPage } from '~/features/sample/auth/successAuth/SuccessAuthPage'
import { ErrorAuthPage } from '~/features/sample/auth/errorAuth/ErrorAuthPage'

const Page: VFC = () => {
  const { user } = useAuth()
  return user ? <SuccessAuthPage /> : <ErrorAuthPage />
}

export default Page
