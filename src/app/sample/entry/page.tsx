'use client'

import React, { VFC } from 'react'
import { SampleEntryPage } from '~/features/sample/entry/SampleEntryPage'

const Page: VFC = () => <SampleEntryPage />

export default Page
