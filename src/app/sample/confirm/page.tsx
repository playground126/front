'use client'

import React, { VFC } from 'react'
import { SampleConfirmPage } from '~/features/sample/confirm/SampleConfirmPage'
import { useSamplePageSession } from '~/features/sample/shared/useSamplePage'

// TODO 共通化を検討
const Page: VFC = () => {
  const samplePageSession = useSamplePageSession()

  return <SampleConfirmPage user={samplePageSession.user} />
}

export default Page
