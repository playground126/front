'use client'

import React, { VFC, Suspense } from 'react'
import dynamic from 'next/dynamic'

// !SSRを防ぐため、利用
// エラー`error hydration failed because the initial ui does not match what was rendered on the server.`の解決のため
// 当記事を参考に実装(https://zenn.dev/takewell/articles/5ee9530eedbeb82e4de7)
// const SampleFetch = dynamic(() => import('~/features/sample/fetch'), {
//   ssr: false,
// })

const Page: VFC = () => {
  // const { data } = useSampleFetch()

  return (
    <Suspense fallback={<p>loading</p>}>
      {/* <SampleFetch image={data} /> */}
      {/* <SampleFetch /> */}
      修正中
    </Suspense>
  )
}

export default Page
