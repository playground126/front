'use client'

import React from 'react'
import Link from 'next/link'
// import type { NextPage } from 'next'
import { Stack } from '@mui/material'

const Home = () => {
  return (
    <>
      <div>app配下のホーム画面</div>
      <Stack>
        <Link href="/sample/entry">入力画面</Link>
        <Link href="/sample/fetch">API呼び出し</Link>
        <Link href="/sample/auth">認証画面</Link>
      </Stack>
    </>
  )
}

export default Home
