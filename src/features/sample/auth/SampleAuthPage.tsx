/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { VFC } from 'react'
import Amplify, { Auth } from 'aws-amplify'
import { useAuth } from '~/features/shared/auth/useAuth'
import { Button } from '@mui/material'
import { useRouter } from 'next/navigation'

// 以下を参考に作成
// https://dev.classmethod.jp/articles/react-cognito-signin/

Amplify.configure({
  Auth: {
    region: 'ap-northeast-1',
    userPoolId: 'ap-northeast-1_gSRP3JTLp',
    userPoolWebClientId: '6g1un77q4b3dnuprsoohq7jc80',
    oauth: {
      domain: 'react-example20220227.auth.ap-northeast-1.amazoncognito.com',
      scope: ['openid'],
      redirectSignIn: 'http://localhost:3000/sample/auth',
      redirectSignOut: 'http://localhost:3000/sample/auth',
      responseType: 'code',
    },
  },
})

export const SampleAuthPage: VFC = () => {
  const { user } = useAuth()
  const router = useRouter()
  return user ? (
    <div>
      <p>サインイン済み</p>
      <p>ユーザー名: {user.username}</p>
      <Button onClick={() => Auth.signOut()}>Sign Out</Button>
      <Button onClick={() => router.push('/sample/auth/successAuth')}>
        認証確認
      </Button>
    </div>
  ) : (
    <div>
      <p>サインインする</p>
      <Button onClick={() => Auth.federatedSignIn()}>Sign In</Button>
      <Button onClick={() => router.push('/sample/auth/successAuth')}>
        認証確認
      </Button>
    </div>
  )
}
