import React, { VFC } from 'react'
import { Button } from '@mui/material'
import { useRouter } from 'next/navigation'

export const SuccessAuthPage: VFC = () => {
  const router = useRouter()
  return (
    <>
      <div>認証が成功しています。</div>
      <Button onClick={() => router.push('/sample/auth')}>戻る</Button>
    </>
  )
}
