import React, { VFC } from 'react'
import { Button } from '@mui/material'
import { useRouter } from 'next/navigation'

export const ErrorAuthPage: VFC = () => {
  const router = useRouter()
  return (
    <>
      <div>認証エラー。認証してください。</div>
      <Button onClick={() => router.push('/sample/auth')}>戻る</Button>
    </>
  )
}
