import React, { VFC, Suspense, useState, useEffect, SVGProps } from 'react'
import { Box, Typography, Stack, Button, CircularProgress } from '@mui/material'
import { Formik, Form } from 'formik'
import useSWR, { useSWRConfig, mutate } from 'swr'

// !Suspenseに関する参考記事:https://zenn.dev/uhyo/books/react-concurrent-handson
function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

// Suspenseの動作確認用コンポーネント
const AlwaysSuspend: React.VFC = () => {
  console.log('AlwaysSuspend is rendered')

  // eslint-disable-next-line @typescript-eslint/no-throw-literal
  throw sleep(1000)
}

// Suspenseの動作確認用コンポーネント
const SometimesSuspend: React.VFC = () => {
  if (Math.random() < 0.5) {
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    throw sleep(1000)
  }
  return <p>Hello, world!</p>
}

export type SampleFetchProps = { image?: string }

const fetcher = (url: string) => fetch(url).then((res) => res.json())

const SampleFetch: VFC<SampleFetchProps> = (props) => {
  // const { data } = useSWR(
  //   'https://ghibliapi.herokuapp.com/films/58611129-2dbc-4a81-a72f-77ddfc1b1b49',
  //   fetcher,
  //   { suspense: true },
  // )

  return (
    <Box>
      <Typography variant="h6">Fetch</Typography>
      {/* <AlwaysSuspend /> */}
      {/* <SometimesSuspend /> */}
      {/* <img src={data.image} alt="となりのトトロ" /> */}
      <Formik
        initialValues={{ firstName: '', lastName: '' }}
        onSubmit={() => {
          console.log('submit')
          fetch('https://cat-fact.herokuapp.com/facts')
            .then((res) => console.log(res))
            .catch((e) => console.error(e))
        }}
      >
        <Form>
          <Stack spacing={2}>
            <Box p={2} display="flex" justifyContent="flex-end">
              <Button type="submit" variant="contained">
                Get Submit
              </Button>
            </Box>
          </Stack>
        </Form>
      </Formik>
    </Box>
  )
}

export default SampleFetch
