import React, { VFC } from 'react'
import { Box, Typography, Stack } from '@mui/material'

export type SampleConfirmPageProps = {
  user?: {
    firstName?: string
    lastName?: string
  }
}

export const SampleConfirmPage: VFC<SampleConfirmPageProps> = (props) => {
  return (
    <Box>
      <Typography variant="h6">サンプルページ（確認画面）</Typography>
      <Stack spacing={2}>
        {props.user && (
          <>
            <Typography>名字： {props.user.lastName}</Typography>
            <Typography>名前： {props.user.firstName}</Typography>
          </>
        )}
      </Stack>
    </Box>
  )
}
