import React, { VFC } from 'react'
import { Box, Typography, Stack, Button } from '@mui/material'
import { Formik, Form } from 'formik'
import { useRouter } from 'next/navigation'
import { useSamplePageSession } from '../shared/useSamplePage'
import { FormikTextField } from '~/features/shared/FormikTextField'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const myAwesomePackage = require('@hori/play-package')

export const SampleEntryPage: VFC = () => {
  const samplePageSession = useSamplePageSession()
  const router = useRouter()
  console.log(myAwesomePackage.packageName())
  return (
    <Box>
      <Typography variant="h6">サンプルページ（入力画面）</Typography>
      <Formik
        initialValues={{ firstName: '', lastName: '' }}
        onSubmit={(values) => {
          console.log('submit')
          samplePageSession.setUser(values)
          router.push('/sample/confirm')
        }}
      >
        <Form>
          <Stack spacing={2}>
            <FormikTextField label="性" fieldName="lastName" />
            <FormikTextField label="名" fieldName="firstName" />
            <Box p={2} display="flex" justifyContent="flex-end">
              <Button type="submit" variant="contained">
                送信
              </Button>
            </Box>
          </Stack>
        </Form>
      </Formik>
    </Box>
  )
}
