import React, {
  createContext,
  useState,
  useContext,
  VFC,
  Dispatch,
  SetStateAction,
  ReactNode,
} from 'react'

// コンテキスト作成の参考記事：https://reffect.co.jp/react/react-usecontext-understanding

type User = { firstName?: string; lastName?: string }

export type SamplePageSession = {
  user?: User
  setUser: Dispatch<SetStateAction<User>>
}

const SamplePageSessionContext = createContext<SamplePageSession>(
  undefined as unknown as SamplePageSession,
)

export function useSamplePageSession(): SamplePageSession {
  return useContext(SamplePageSessionContext)
}

export type SamplePageSessionProviderProps = { children: ReactNode }

export const SamplePageSessionProvider: VFC<SamplePageSessionProviderProps> = ({
  children,
}) => {
  const [user, setUser] = useState<User>({})

  const value = {
    user,
    setUser,
  }

  return (
    <SamplePageSessionContext.Provider value={value}>
      {children}
    </SamplePageSessionContext.Provider>
  )
}
