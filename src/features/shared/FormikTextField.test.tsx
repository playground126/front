import React from 'react'
import { render, screen } from '@testing-library/react'
import { FormikTextField } from './FormikTextField'
import { Formik, Form } from 'formik'
import userEvent from '@testing-library/user-event'

describe('SampleTest:FormikTextField', () => {
  it('正常系', () => {
    render(
      <Formik
        initialValues={{ name: '' }}
        onSubmit={() => console.log('submit')}
      >
        <Form>
          <FormikTextField fieldName="name" label="name" />
        </Form>
      </Formik>,
    )
    userEvent.type(screen.getByLabelText('name'), 'サンプル太郎')
    expect(screen.getByDisplayValue('サンプル太郎')).toBeInTheDocument()
  })
})
