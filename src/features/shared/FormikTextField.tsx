import React, { VFC } from 'react'
import { useField } from 'formik'
import { TextField } from '@mui/material'

export type FormikTextFieldProps = {
  fieldName: string
  label: string
  disabled?: boolean
}

export const FormikTextField: VFC<FormikTextFieldProps> = ({
  fieldName,
  label,
  disabled = false,
}) => {
  const [field, meta] = useField(fieldName)
  return (
    <TextField
      id={field.name}
      name={field.name}
      value={field.value}
      onChange={field.onChange}
      onBlur={field.onBlur}
      label={label}
      error={meta.touched && !!meta.error}
      helperText={meta.touched && meta.error}
      disabled={disabled}
    />
  )
}
