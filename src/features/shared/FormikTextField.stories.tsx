import React from 'react'
import { Story, Meta } from '@storybook/react'
import { FormikTextField, FormikTextFieldProps } from './FormikTextField'
import { Formik, Form } from 'formik'

export default {
  component: FormikTextField,
  title: 'shared/FormikTextField',
} as Meta

const Template: Story<FormikTextFieldProps> = (args) => (
  // モックのFormik
  // Storybookの`https://storybook.js.org/addons/storybook-formik`でも実装可能。
  <Formik initialValues={{ name: '' }} onSubmit={() => console.log('submit')}>
    <Form>
      <FormikTextField {...args} />
    </Form>
  </Formik>
)

export const Default = Template.bind({})
Default.storyName = '初期状態'
Default.args = {
  fieldName: 'name',
}

export const Archived = Template.bind({})
Archived.storyName = '正常'
Archived.args = {
  fieldName: 'name',
  label: 'テスト',
}

export const Disabled = Template.bind({})
Disabled.storyName = '非活性'
Disabled.args = {
  fieldName: 'name',
  label: 'テスト',
  disabled: true,
}
