import { ChangeEvent, useEffect, useState } from 'react'
import { Auth, Hub } from 'aws-amplify'

export const useAuth = () => {
  const [user, setUser] = useState<any | null>(null)

  const getUser = async () => {
    try {
      const userData = await Auth.currentAuthenticatedUser()
      // デバッグ用
      Auth.currentSession().then((data) => {
        console.log(`token: ${data.getIdToken().getJwtToken()}`)
      })
      console.log(userData)
      return userData
    } catch (e) {
      return console.log('Not signed in')
    }
  }

  useEffect(() => {
    Hub.listen('auth', ({ payload: { event, data } }) => {
      switch (event) {
        case 'signIn':
        case 'cognitoHostedUI':
          getUser().then((userData) => setUser(userData))
          break
        case 'signOut':
          setUser(null)
          break
        case 'signIn_failure':
        case 'cognitoHostedUI_failure':
          console.log('Sign in failure', data)
          break
      }
    })

    getUser().then((userData) => setUser(userData))
  }, [])

  return { user }
}
